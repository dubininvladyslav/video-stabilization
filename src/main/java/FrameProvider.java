import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

public class FrameProvider {
    private VideoCapture capture;
    private Mat frameMatrix = new Mat();
    private double fps;

    public FrameProvider(String pathToVideo) {
        this.capture = new VideoCapture(pathToVideo);
        this.fps = capture.get(Videoio.CAP_PROP_FPS);
    }

    public Mat provide() {
        if (capture.isOpened() && capture.read(frameMatrix)) {
            return frameMatrix;
        }
        return null;
    }

    public double frameRate() {
        return fps;
    }
}
