import org.opencv.core.Mat;
import org.opencv.core.Point;
import utils.Convertors;
import utils.Filers;
import utils.Util;

import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.*;

public class Demo {

    public static void main(String[] args) {
        new Demo().run("C:\\Users\\Charles\\Desktop\\cv\\videos\\top-bottom.mp4");
    }

    private List<Long> times = new ArrayList<>();

    public void run(String videoPath) {
        URL url = getClass().getResource("opencv_java3416.dll");
        System.load(url.getPath());


        FrameProvider frameProvider = new FrameProvider(videoPath);
        VideoStream videoStream = new VideoStream();
        VideoStabilizationProcessor videoStabilization = new VideoStabilizationProcessor();

        int videoSpeed = (int) (1000/ frameProvider.frameRate());


        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                Mat currentFrame = frameProvider.provide();
                if (Objects.isNull(currentFrame)) {
                    System.out.println("Average speed: " +times.stream().mapToLong(Long::longValue).sum() / times.size() + "ms.");
                    System.exit(0);
                    return;
                }

                Mat grayImage = Convertors.toGray(currentFrame);
                Mat clearImage = Filers.medianBlur(grayImage, 15);

                long startTime = System.currentTimeMillis();
                Point processingResult = videoStabilization.process(clearImage);
                times.add(System.currentTimeMillis() - startTime);

                Point offsetOfCenter = findCenterOffset(processingResult, currentFrame);
                Mat result = addFrameOnBackground(currentFrame, offsetOfCenter);
                BufferedImage image = Convertors.toImage(result);
                videoStream.updateImage(image);

            }
        }, 0, videoSpeed);
    }

    private Point findCenterOffset(Point coordinatesOfCenter, Mat frame) {
        return new Point(frame.size().width / 2 - coordinatesOfCenter.x, frame.size().height / 2 - coordinatesOfCenter.y);
    }
    public static Mat addFrameOnBackground(Mat img,  Point offsetOfCenter) {
        double scale = 0.5;
        double normalizeXMove = (offsetOfCenter.x * scale);
        double normalizeYMove = (offsetOfCenter.y * scale);

        Mat background = Mat.zeros(img.size(), img.type());
        Mat frame = Util.resize(img, scale);

        double xCenterPosition = background.size().width / 2 - frame.size().width / 2;
        double yCenterPosition = background.size().height / 2 - frame.size().height / 2;

        return Util.concat(frame, background, new Point(xCenterPosition + normalizeXMove, yCenterPosition + normalizeYMove));
    }
}
