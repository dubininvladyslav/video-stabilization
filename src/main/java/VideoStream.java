import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

public class VideoStream {
    private JFrame jFrame = new JFrame("Stream BufferedImage");
    private JLabel imageLabel;

    public VideoStream() {
        jFrame.setDefaultCloseOperation(jFrame.EXIT_ON_CLOSE);
        jFrame.setLayout(new BorderLayout());

        imageLabel = new JLabel();
        jFrame.add(imageLabel, BorderLayout.CENTER);
        jFrame.pack();
        jFrame.setLocationRelativeTo(null);
        jFrame.setVisible(true);

        JPanel buttonPanel = new JPanel(new BorderLayout());

        JButton exitButton = new JButton("Exit");
        exitButton.addActionListener(e -> System.exit(0));
        buttonPanel.add(exitButton, BorderLayout.CENTER);

        jFrame.getContentPane().add(buttonPanel, BorderLayout.BEFORE_FIRST_LINE);

        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice device = env.getDefaultScreenDevice();
        device.setFullScreenWindow(jFrame);
    }

    public void updateImage(BufferedImage image) {
        imageLabel.setIcon(new ImageIcon(image));
    }
}