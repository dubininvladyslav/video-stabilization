import org.opencv.core.*;
import utils.Filers;
import utils.Util;

import java.util.List;

public class VideoStabilizationProcessor {

    public Point process(final Mat frame) {;

        Mat thresholdImage = Filers.thresholding(frame, 200, 255);

        List<MatOfPoint> contours = Util.findContours(thresholdImage);

        // TODO find first contur
        return contours.stream().findFirst()
                .map(Util::findCenter)
                .orElseGet(Point::new);
    }
}
