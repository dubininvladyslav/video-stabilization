package utils;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import java.util.ArrayList;
import java.util.List;

public class Util {

    public static Point findCenter(MatOfPoint matOfPoint) {
        MatOfPoint2f contour2f = new MatOfPoint2f(matOfPoint.toArray());
        Moments moments = Imgproc.moments(contour2f);

        double m00 = moments.get_m00();
        if(m00 != 0) {
            return new Point((moments.get_m10() / m00), (moments.get_m01() / m00));
        }
        return new Point();
    }

    public static List<MatOfPoint> findContours(Mat thresholdFrame) {
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(thresholdFrame, contours, new Mat(), Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        return contours;
    }

    public static Mat resize(Mat img, double scale) {
        Mat resizedImg = new Mat();
        Size newSize = new Size(img.cols() * scale, img.rows() * scale);
        Imgproc.resize(img, resizedImg, newSize, 0, 0, Imgproc.INTER_AREA);
        return resizedImg;
    }

    public static Mat concat(Mat front, Mat back, Point position) {
        front.copyTo(back.submat((int) position.y, (int) (front.rows() + position.y), (int) position.x, (int) (front.cols() + position.x)));
        return back;
    }
}
