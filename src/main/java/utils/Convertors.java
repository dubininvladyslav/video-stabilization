package utils;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.awt.image.BufferedImage;

public final class Convertors {
    public static BufferedImage toImage(Mat matrixFrame) {
        int cols = matrixFrame.cols();
        int rows = matrixFrame.rows();
        int elemSize = (int) matrixFrame.elemSize();
        byte[] data = new byte[cols * rows * elemSize];
        int type;
        matrixFrame.get(0, 0, data);
        switch (matrixFrame.channels()) {
            case 1:
                type = BufferedImage.TYPE_BYTE_GRAY;
                break;
            case 3:
                type = BufferedImage.TYPE_3BYTE_BGR;
                break;
            case 4:
                type = BufferedImage.TYPE_4BYTE_ABGR;
                break;
            default:
                return null;
        }
        BufferedImage image = new BufferedImage(cols, rows, type);
        image.getRaster().setDataElements(0, 0, cols, rows, data);
        return image;
    }

    public static Mat toGray(Mat matrixFrame) {
        Mat grayImage = new Mat(matrixFrame.rows(), matrixFrame.cols(), CvType.CV_8UC1);
        Imgproc.cvtColor(matrixFrame, grayImage, Imgproc.COLOR_BGR2GRAY);
        return grayImage;
    }

}
