package utils;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public final class Filers {
    public static Mat thresholding(Mat matrixFrame, int thresh, int maxVal) {
        Mat result = new Mat();
        Imgproc.threshold(matrixFrame, result, thresh, maxVal, Imgproc.THRESH_BINARY_INV);
        return result;
    }

    public static Mat medianBlur(Mat grayImage, int value) {
        Mat dest = new Mat();
        Imgproc.medianBlur(grayImage, dest, value);
        return dest;
    }
}
